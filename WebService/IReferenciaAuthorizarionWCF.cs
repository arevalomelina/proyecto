﻿using Proyecto.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WebService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IReferenciaAuthorizarionWCF" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IReferenciaAuthorizarionWCF
    {
        [OperationContract]
        List<Menu> ObtenerMenu(string username);
    }
}
