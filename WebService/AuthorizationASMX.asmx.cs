﻿using Proyecto.Modelo;
using Proyecto.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebService
{
    /// <summary>
    /// Descripción breve de AuthorizationASMX
    /// </summary>
    [WebService(Namespace = "http://cedesistemas.edu/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class AuthorizationASMX : System.Web.Services.WebService
    {

        [WebMethod]
        public List<Menu> ObtenerMenu(string username)
        {
            return new SecurityFachada().ObtenerMenu(username);
        }

        [WebMethod]
        public int sumar(int a, int b)
        {
            return a+b;
        }
    }
}
