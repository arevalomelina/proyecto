﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Proyecto.Modelo;
using Proyecto.Modelo.Entidades;

namespace WebService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "ReferenciaAuthorizarionWCF" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione ReferenciaAuthorizarionWCF.svc o ReferenciaAuthorizarionWCF.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class ReferenciaAuthorizarionWCF : IReferenciaAuthorizarionWCF
    {
        public List<Menu> ObtenerMenu(string username)
        {
            return new SecurityFachada().ObtenerMenu(username);
        }
    }
}
