﻿using Proyecto.AplicacionEscritorio.Utilidades;
using Proyecto.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.AplicacionEscritorio
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text;
            string password = txtPassword.Text;

            SecurityFachada fachada = new SecurityFachada();

            bool isValid = fachada.AutenticarUsuario(username, password);

            if(isValid)
            {
                SessionHelper.UserName = username;
                Principal frmPrincipal = new Principal();
                frmPrincipal.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Usuario no es valido");
            }
        }
    }
}
