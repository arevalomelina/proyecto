﻿using Proyecto.AplicacionEscritorio.Utilidades;
using Proyecto.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.AplicacionEscritorio
{
    public partial class Principal : Form
    {
        private int childFormNumber = 0;
        List<Modelo.Entidades.Menu> menus;

        public Principal()
        {
            InitializeComponent();
            menus = new SecurityFachada().ObtenerMenu(SessionHelper.UserName);

            List<Modelo.Entidades.Menu> menuPadres = menus.Where(p => p.ParentId == 0).ToList();
            CrearMenu(menuPadres);
        }

        private void CrearMenu(List<Modelo.Entidades.Menu> menus)
        {
            foreach (var item in menus)
            {
                ToolStripMenuItem menuItem = new ToolStripMenuItem(item.Nombre);
                List<Modelo.Entidades.Menu> menuHijos = this.menus.Where(p => p.ParentId == item.AsegurableId).ToList();
                CrearMenu(menuItem, menuHijos);
                menuPrincipal.Items.Add(menuItem);
            }

        }

        private void CrearMenu(ToolStripMenuItem menuItem, List<Modelo.Entidades.Menu> menus)
        {
            foreach (var item in menus)
            {
                ToolStripMenuItem menuItemHijo = new ToolStripMenuItem(item.Nombre);
                menuItemHijo.Click += MenuItemHijo_Click;
                menuItemHijo.Tag = item;
                menuItem.DropDownItems.Add(menuItemHijo);
            }

        }

        private void MenuItemHijo_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem)
            {
                var menuOption = sender as ToolStripMenuItem;
                if (menuOption.Tag != null)
                {
                    if (menuOption.Tag is Modelo.Entidades.Menu)
                    {

                        var menur = menuOption.Tag as Modelo.Entidades.Menu;
                        if (string.IsNullOrWhiteSpace(menur.Ruta))
                            this.Close();
                        Form childForm = (Form)Assembly.GetExecutingAssembly().CreateInstance(menur.Ruta);
                        childForm.MdiParent = this;
                        childForm.Show();
                    }
                }


            }
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Ventana " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }



        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }
    }
}
