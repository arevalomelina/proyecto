﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.AplicacionEscritorio.ControlUsuarios
{
    public class EditableGrid:DataGridView
    {
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if(keyData==Keys.Enter)
            {
                // return the tab key
                return ProcessDialogKey(Keys.Tab);
            }
            else
            // return the key to the base class if not used.
            return base.ProcessDialogKey(keyData);
        }


    }
}
