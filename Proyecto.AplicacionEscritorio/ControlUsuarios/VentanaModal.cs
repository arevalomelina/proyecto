﻿using Proyecto.AplicacionEscritorio.Utilidades;
using Proyecto.Modelo;
using Proyecto.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.AplicacionEscritorio.ControlUsuarios
{
    public partial class VentanaModal : Form
    {


        public VentanaModal()
        {
            InitializeComponent();
        }

        public VentanaModal(TipoBusqueda tipo)
        {
            InitializeComponent();
            this.Tipo = tipo;
            lblTitulo.Text = lblTitulo.Text + tipo.ToString();

            if (tipo == TipoBusqueda.Clientes)
            {
                List<Cliente> lstGrid = new ModeloFachada().GetClientes();
                dgvLista.DataSource = lstGrid;
                dgvLista.Refresh();
            }
            else if (tipo == TipoBusqueda.Productos)
            {
                List<Productos> lstGrid = new ModeloFachada().GetProductos();
                dgvLista.DataSource = lstGrid;
                dgvLista.Refresh();
            }

        }

        public Cliente SelectedClient { get; set; }
        public Productos SelectedProduct { get; set; }
        TipoBusqueda Tipo { get; set; }


        private void button1_Click(object sender, EventArgs e)
        {
            if (Tipo == TipoBusqueda.Clientes)
            {
                Cliente currentObject = (Cliente)dgvLista.CurrentRow.DataBoundItem;
                this.SelectedClient = currentObject;
            }
            else if (Tipo == TipoBusqueda.Productos)
            {
                Productos currentObject = (Productos)dgvLista.CurrentRow.DataBoundItem;
                this.SelectedProduct = currentObject;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
