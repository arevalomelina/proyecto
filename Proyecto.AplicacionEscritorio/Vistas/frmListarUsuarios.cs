﻿using Proyecto.Modelo;
using Proyecto.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.AplicacionEscritorio.Vistas
{
    public partial class frmListarUsuarios : Form
    {
        ModeloFachada Fachada = new ModeloFachada();
        List<Usuario> lstUsuarios = new List<Usuario>(); 


        public frmListarUsuarios()
        {
            InitializeComponent();
            lstUsuarios = Fachada.ListarUsuarios();
            dgvUsuarios.DataSource = lstUsuarios;
            dgvUsuarios.Refresh();
        }

        private void frmListarUsuarios_Load(object sender, EventArgs e)
        {
            this.Text = "ListarUsuarios v1.2";
        }
    }
}
