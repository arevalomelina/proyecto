﻿using Proyecto.AplicacionEscritorio.ControlUsuarios;
using Proyecto.Modelo;
using Proyecto.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.AplicacionEscritorio.Vistas
{
    public partial class MaestroDetalle : Form
    {
        private EditableGrid dgvDetalle;
        private Cliente cliente = new Cliente();
        public MaestroDetalle()
        {
            InitializeComponent();
            dgvDetalle = new EditableGrid();
            dgvDetalle.CellEndEdit += DgvDetalle_CellEndEdit;
            dgvDetalle.CellClick += DgvDetalle_CellClick;
            dgvDetalle.CellPainting += DgvDetalle_CellPainting;
            ConstruirGrilla();
            lblIVA.Text = "IVA " + ConfigurationManager.AppSettings["iva"].ToString() + "%:";
        }

        private void DgvDetalle_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex == dgvDetalle.NewRowIndex || e.RowIndex < 0)
                return;

            if (e.ColumnIndex == dgvDetalle.Columns["dataGridViewDeleteButton"].Index)
            {
                if (e.RowIndex > -1)
                {
                    Image img = Properties.Resources.if_minus_1645995;

                    e.PaintContent(e.CellBounds);
                    e.Graphics.DrawImage(img, e.CellBounds.Location);

                    e.Handled = true;

                }
            }
        }

        private void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            VentanaModal popUpLista = new VentanaModal(Utilidades.TipoBusqueda.Clientes);
            var result = popUpLista.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                this.cliente = popUpLista.SelectedClient;
                lblNombre.Text = popUpLista.SelectedClient.nombre;
                txtCliente.Text = popUpLista.SelectedClient.ClienteId.ToString();
            }

        }

        private void txtCliente_TextChanged(object sender, EventArgs e)
        {
            this.cliente = new ModeloFachada().GetCliente(txtCliente.Text);

            if (this.cliente == null)
            {
                MessageBox.Show("El cliente no existe");
            }
            else
            {
                if (this.cliente.ClienteId == 0)
                    MessageBox.Show("El cliente no existe");
                else
                    lblNombre.Text = this.cliente.nombre;
            }
        }

        private void ConstruirGrilla()
        {
            dgvDetalle.ColumnCount = 6;

            dgvDetalle.Columns[0].HeaderText = "Código";
            dgvDetalle.Columns[1].HeaderText = "Descripción";
            dgvDetalle.Columns[2].HeaderText = "P.Unitario";
            dgvDetalle.Columns[3].HeaderText = "Cantidad";
            dgvDetalle.Columns[4].HeaderText = "Total";

            dgvDetalle.Columns[0].Width = 60;
            dgvDetalle.Columns[1].Width = 300;
            dgvDetalle.Columns[2].Width = 80;
            dgvDetalle.Columns[3].Width = 80;
            dgvDetalle.Columns[4].Width = 80;


            //codigo
            dgvDetalle.Columns[0].DefaultCellStyle.BackColor = Color.LemonChiffon;

            //Descripcion
            dgvDetalle.Columns[1].DefaultCellStyle.BackColor = Color.LemonChiffon;

            //Precio Unitario
            dgvDetalle.Columns[2].DefaultCellStyle.Format = "N0";
            dgvDetalle.Columns[2].DefaultCellStyle.NullValue = "0";
            dgvDetalle.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvDetalle.Columns[2].DefaultCellStyle.BackColor = Color.LemonChiffon;

            //Cantidad
            dgvDetalle.Columns[3].DefaultCellStyle.NullValue = "0";
            dgvDetalle.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvDetalle.Columns[3].DefaultCellStyle.BackColor = Color.LemonChiffon;

            //Total
            dgvDetalle.Columns[4].DefaultCellStyle.Format = "N0";
            dgvDetalle.Columns[4].DefaultCellStyle.NullValue = "0";
            dgvDetalle.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvDetalle.Columns[4].ReadOnly = true;

            dgvDetalle.BackgroundColor = Color.LightSteelBlue;

            dgvDetalle.Columns[5].Visible = false;


            var deleteButton = new DataGridViewButtonColumn();
            deleteButton.Name = "dataGridViewDeleteButton";
            deleteButton.HeaderText = "";
            deleteButton.Text = "";
            deleteButton.UseColumnTextForButtonValue = true;
            deleteButton.Width = 25;

            dgvDetalle.Columns.Add(deleteButton);
            panelDetailGrid.Controls.Add(dgvDetalle);
            dgvDetalle.Dock = DockStyle.Fill;
            dgvDetalle.AlternatingRowsDefaultCellStyle.BackColor = Color.AliceBlue;
        }

        private void DgvDetalle_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)//codigo
            {
                Productos p = new Productos();
                VentanaModal popUpLista = new VentanaModal(Utilidades.TipoBusqueda.Productos);
                var result = popUpLista.ShowDialog(this);
                if (result == DialogResult.OK)
                {
                    p = popUpLista.SelectedProduct;
                }

                if (p != null)
                {

                    //como el producto existe, muestra sus datos en las respectivas celdas
                    dgvDetalle.Rows[e.RowIndex].Cells[0].Value = p.Codigo;
                    //como el producto existe, muestra sus datos en las respectivas celdas
                    dgvDetalle.Rows[e.RowIndex].Cells[2].Value = p.Precio;
                    dgvDetalle.Rows[e.RowIndex].Cells[1].Value = p.Descripcion;
                    dgvDetalle.Rows[e.RowIndex].Cells[5].Value = p.ProductoId;
                    //Calcula cantidad * precio
                    Multiplicar(e.RowIndex);
                    dgvDetalle.CurrentCell = dgvDetalle.Rows[e.RowIndex].Cells[3];
                }
                return;
            }
            else if (e.ColumnIndex == 5)//boton
            {
                int row = dgvDetalle.CurrentCell.RowIndex;
                if (!dgvDetalle.Rows[row].IsNewRow)
                    dgvDetalle.Rows.RemoveAt(row);
            }

        }

        private void DgvDetalle_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //si la celda que se ha modificado es la que contiene el precio de venta
            if (e.ColumnIndex == 2)//precio
            {
                decimal nuevoValor;
                if (!decimal.TryParse(dgvDetalle.CurrentCell.Value.ToString(), out nuevoValor))
                {
                    dgvDetalle.CurrentCell = dgvDetalle.CurrentRow.Cells[2];
                    dgvDetalle.CurrentCell.ErrorText = "Solo números positivos mayores que cero.";
                    dgvDetalle.CurrentCell.Value = 0;
                    dgvDetalle.Rows[e.RowIndex].Cells[4].Value = 0;
                    return;
                }
                if ((decimal)dgvDetalle.CurrentCell.Value > 0)
                {
                    dgvDetalle.CurrentCell.ErrorText = String.Empty;
                    if (decimal.TryParse(dgvDetalle.CurrentCell.Value.ToString(), out nuevoValor))
                    {
                        Multiplicar(e.RowIndex);
                    }
                    else
                    {
                        dgvDetalle.CurrentCell.ErrorText = "Solo números positivos mayores que cero.";
                        dgvDetalle.CurrentCell.Value = 0;
                        dgvDetalle.Rows[e.RowIndex].Cells[4].Value = 0;
                    }
                }
            }

            // si la celda que se ha modificado es la que contien la cantidad
            else if (e.ColumnIndex == 3)//'cantidad
            {
                decimal nuevoValor;
                if (!decimal.TryParse(dgvDetalle.CurrentCell.Value.ToString(), out nuevoValor))
                {
                    dgvDetalle.CurrentCell = dgvDetalle.CurrentRow.Cells[3];
                    dgvDetalle.CurrentCell.ErrorText = "Solo números positivos mayores que cero.";
                    dgvDetalle.CurrentCell.Value = 0;
                    dgvDetalle.Rows[e.RowIndex].Cells[4].Value = 0;
                    return;
                }
                if (nuevoValor > 0)
                {
                    dgvDetalle.CurrentCell.ErrorText = String.Empty;
                    Multiplicar(e.RowIndex);
                }
                else
                {
                    dgvDetalle.CurrentCell.ErrorText = "Solo números positivos mayores que cero.";
                    dgvDetalle.CurrentCell.Value = 0;
                    dgvDetalle.Rows[e.RowIndex].Cells[4].Value = 0;
                }

            }
            Sumar();
        }

        private void Multiplicar(int fila)
        {
            if (dgvDetalle.Rows[fila].Cells[3].Value != null && dgvDetalle.Rows[fila].Cells[2].Value != null)
                dgvDetalle.Rows[fila].Cells[4].Value = decimal.Parse(dgvDetalle.Rows[fila].Cells[3].Value.ToString()) * (decimal)dgvDetalle.Rows[fila].Cells[2].Value;
        }

        private void Sumar()
        {

            decimal suma = 0;
            decimal iva = 0;
            decimal total = 0;
            decimal factor = decimal.Parse(ConfigurationManager.AppSettings["iva"].ToString()) / 100;
            try
            {
                for (int i = 0; i <= dgvDetalle.Rows.Count - 2; i++)
                    suma += (decimal)dgvDetalle.Rows[i].Cells[4].Value;

                iva = suma * factor;
                total = suma + iva;


                txtSumaAfecto.Text = String.Format("{0:n0}", suma);
                txtSumaIva.Text = String.Format("{0:n0}", iva);
                txtSumaTotal.Text = String.Format("{0:n0}", total);
            }
            catch (Exception ex) { }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Maestro maestro = new Maestro();
            List<Detalle> lstDetalles = new List<Detalle>();

            maestro.ClientId = int.Parse(txtCliente.Text);
            maestro.fecha = dateTimePicker1.Value;
            maestro.iva = decimal.Parse(txtSumaIva.Text);
            maestro.ivaAplicado = int.Parse(ConfigurationManager.AppSettings["iva"].ToString());
            maestro.total = decimal.Parse(txtSumaTotal.Text);
            maestro.vendedor = comboBox1.Text;

            foreach (DataGridViewRow row in dgvDetalle.Rows)
            {
                if (!row.IsNewRow)
                    lstDetalles.Add(new Detalle()
                    {
                        ProductoId = int.Parse(row.Cells[5].Value.ToString()),
                        Cantidad = int.Parse(row.Cells[3].Value.ToString()),
                        precio = decimal.Parse(row.Cells[2].Value.ToString())
                    });
            }
        }
    }
}
