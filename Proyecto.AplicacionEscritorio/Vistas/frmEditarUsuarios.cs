﻿using Proyecto.AplicacionEscritorio.Utilidades;
using Proyecto.Modelo;
using Proyecto.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.AplicacionEscritorio.Vistas
{
    public partial class frmEditarUsuarios : Form
    {
        public frmEditarUsuarios()
        {
            InitializeComponent();
        }

        private void pbFotoPerfil_Click(object sender, EventArgs e)
        {

        }

        private void cmbRoles_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void lblValPassword_Click(object sender, EventArgs e)
        {

        }

        private void lblValUser_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnUploadImage_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = txtUserName.Text + "3.jpeg";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pbFotoPerfil.Image.Save(saveFileDialog1.FileName, ImageFormat.Jpeg);
            }
            //string path = @"D:\ImagenesGuardadas\" + txtUserName.Text + ".jpeg";
            // pbFotoPerfil.Image.Save(path, ImageFormat.Jpeg);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            ModeloFachada fachada = new ModeloFachada();
            Usuario objUsuario = fachada.obtenerUsuario(txtUserName.Text);
            txtUserName.Text = objUsuario.Username;
            pbFotoPerfil.Image = new ImagenHelper().byteArrayToImage(objUsuario.FotoPerfil);
        }
    }
}
