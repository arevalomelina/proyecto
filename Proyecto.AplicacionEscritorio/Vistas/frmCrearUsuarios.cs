﻿using Proyecto.AplicacionEscritorio.Utilidades;
using Proyecto.Modelo;
using Proyecto.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.AplicacionEscritorio.Vistas
{
    public partial class frmCrearUsuarios : Form
    {
        public frmCrearUsuarios()
        {
            InitializeComponent();
            List<Rol> lstRoles = new ModeloFachada().ListarRoles();
            cmbRoles.DataSource = lstRoles;
            cmbRoles.ValueMember = "RolId";
            cmbRoles.DisplayMember="Name";
            cmbRoles.Refresh();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (ValidarFormulario())
            {
                ModeloFachada fachada = new ModeloFachada();
                int rolId = (int)cmbRoles.SelectedValue;
                Usuario objUsuario = new Usuario();
                objUsuario.Username = txtUserName.Text;
                objUsuario.Password = txtPassword.Text;
                objUsuario.FotoPerfil = new ImagenHelper().imageToByteArray(pbFotoPerfil.Image);
                bool resultado = fachada.CrearUsuarios(objUsuario, rolId);

                if (resultado)
                {
                    lblMensaje.Text = "Usuario creado exitosamente";
                    lblMensaje.ForeColor = Color.Green;
                }
                else
                {
                    lblMensaje.Text = "Hubo un problema creando el usuario. Reintente";
                    lblMensaje.ForeColor = Color.Red;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtUserName.Text = "";
            txtPassword.Text = "";
            lblMensaje.Text = "";
            lblMensaje.ForeColor = Color.Black;
        }

        private bool ValidarFormulario()
        {
            Validaciones validaciones = new Validaciones();
            string username = txtUserName.Text.Trim();
            string password = txtPassword.Text.Trim();

            string mensaje = string.Empty;
            bool resultado = true;

            if (string.IsNullOrWhiteSpace(username))
            {
                mensaje = mensaje + "El nombre de usuario es requerido";
                lblValUser.Visible = true;
                resultado = false;
            }
            else if (!validaciones.ValidarUsername(username))
            {
                mensaje = mensaje + "El nombre de usuario ya existe";
                resultado = false;
                lblValUser.Visible = true;
            }
            else
            {
                lblValUser.Visible = false;
            }



            if (string.IsNullOrWhiteSpace(password))
            {
                mensaje = mensaje + "La contraseña es requerida";
                resultado = false;
                lblValPassword.Visible = true;
            }
            else if (!validaciones.ValidarPassword(password))
            {
                mensaje = mensaje + "la contraseña no cumple con la politica de minimo 8 caracteres....";
                resultado = false;
                lblValPassword.Visible = true;
            }
            else
                lblValPassword.Visible = false;

            lblMensaje.Text = mensaje;
            lblMensaje.ForeColor = Color.Red;
            return resultado;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnUploadImage_Click(object sender, EventArgs e)
        {
            ofdAbrirImagen.Filter = "Images Files|*.jpg;*.jpeg;*.png;";
            ofdAbrirImagen.Title = "Seleccionar Imagen";
            ofdAbrirImagen.FileName = "";
            if (ofdAbrirImagen.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string selectedFile = ofdAbrirImagen.FileName;
                pbFotoPerfil.ImageLocation = selectedFile;
            }
        }
    }
}
