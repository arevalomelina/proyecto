﻿using Proyecto.AplicacionEscritorio.Utilidades;
using Proyecto.Modelo;
using Proyecto.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.AplicacionEscritorio.Vistas
{
    public partial class frmTipos : Form
    {
        public frmTipos()
        {
            InitializeComponent();
        }

        private void btnCrear_Click(object sender, EventArgs e)
        {
            int cantidad = 0;
            List<Tipo> lstTipos = new List<Tipo>();
            Tipo tipo;
            bool resp = false;
            if(int.TryParse(txtCantidad.Text,out cantidad))
            {
                
                for(int i = 25; i< cantidad + 25;i++)
                {
                    tipo = new Tipo();
                    tipo.TipoId = i;
                    tipo.Descrpcion = "descripcion tipo " + i.ToString();
                    lstTipos.Add(tipo);
                }

                resp = new ModeloFachada().AgregarTiposXml(lstTipos);

                if(resp)
                {
                    MessageBox.Show("Registros insertados correctamente");
                }
                else
                {
                    MessageBox.Show("No se pudo realizar la insercion");
                }
            }
            else
            {
                lblError.Text = "El valor debe ser un numero";
            }
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            SessionHelper.ClearSpace(this);
        }

        private void btnHambre_Click(object sender, EventArgs e)
        {
            SessionHelper.ClearSpace(gbHambre);
        }
    }
}
