﻿using Proyecto.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.AplicacionEscritorio
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Images Files|*.jpg;*.jpeg;*.png;";
            openFileDialog1.Title = "Select an Image";

            // Show the Dialog.  
            // If the user clicked OK in the dialog and  
            // a .CUR file was selected, open it.  
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // Assign the cursor in the Stream to the Form's Cursor property.  
                string selectedFile = openFileDialog1.FileName;
                pictureBox1.ImageLocation = selectedFile;
            }
        }

        byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ModeloFachada fachada = new ModeloFachada();
            byte[] imagen = imageToByteArray(pictureBox1.Image);
            fachada.GuardarImagen(pictureBox1.ImageLocation, imagen);
        }

       
        private void button3_Click_1(object sender, EventArgs e)
        {
            ModeloFachada fachada = new ModeloFachada();
            byte[] imagen = fachada.ObtenerImagen();
            MemoryStream ms = new MemoryStream(imagen);
            Image returnImage = Image.FromStream(ms);
            pictureBox1.Image = returnImage;
        }
    }
}
