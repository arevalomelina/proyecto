﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Principal.Master" AutoEventWireup="true" CodeBehind="Formulario1.aspx.cs" Inherits="Proyecto.WebApplication.Forms.Formulario1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Digite su nombre</h1>
    <asp:TextBox runat="server"></asp:TextBox>
    <asp:Button runat="server" Text="Button" />
    <div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Warning!</strong> Better check yourself, you're not looking too good.
    </div>

    <form role="form">
        <div class="form-group">
            <label class="control-label" for="exampleInputEmail">Email address</label>
            <input runat="server" type="email" class="form-control" id="exampleInputEmail" placeholder="Enter email">
            <input type="password" class="form-control" id="examplePassword" placeholder="Enter email">
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox">
                Remember me
            </label>
        </div>
        <asp:Button ID="Button1" runat="server" class="btn btn-primary" Text="mySubmit" OnClick="Button1_Click" />
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</asp:Content>
