﻿using Proyecto.consola.ReferenciaAuthorizationAsmx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.consola
{
    class Program
    {
        static void Main(string[] args)
        {
            using(var servicio = new ReferenciaAuthorizationAsmx.AuthorizationASMXSoapClient())
            {
                List<Menu> lstMenu = servicio.ObtenerMenu("marevalo").ToList();
                int suma = servicio.sumar(1, 2);
            }

            using (var servicio = new ReferenciaAuthorizationWCF.ReferenciaAuthorizarionWCFClient())
            {
                List<ReferenciaAuthorizationWCF.Menu> lstmenu = servicio.ObtenerMenu("marevalo").ToList();
            }
        }
    }
}
