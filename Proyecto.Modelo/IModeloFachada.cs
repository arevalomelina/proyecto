﻿using Proyecto.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Modelo
{
    [ServiceContract]
    interface IModeloFachada
    {
        bool CrearUsuarios(Usuario objUsuario, int RolId);
        bool ValidarNombreUsuario(string username);
        List<Usuario> ListarUsuarios();
        Usuario obtenerUsuario(string username);
        List<Rol> ListarRoles();
        bool AgregarTiposBatch(List<Tipo> lstTipos);
        bool AgregarTiposXml(List<Tipo> lstTipos);
        Cliente GetCliente(string cedula);
        List<Productos> GetProductos();
        [OperationContract]
        List<Cliente> GetClientes();
    }

}
