﻿using Proyecto.Modelo.Datos;
using Proyecto.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Modelo.Logica
{
    internal class RolBL
    {
        internal List<Rol> ListarRoles()
        {
            string sqlString = @"SELECT [RolId],[Descripcion] FROM[security].[Roles]";

            DataTable dtRoles = new Dao().GetDataTable(sqlString);

            List<Rol> lstRoles = new List<Rol>();
            Rol rol;
            foreach (DataRow fila in dtRoles.Rows)
            {
                rol = new Rol();
                rol.RolId = int.Parse(fila["RolId"].ToString());
                rol.Name = fila[1].ToString();


                lstRoles.Add(rol);
            }

            return lstRoles;
        }

    }
}
