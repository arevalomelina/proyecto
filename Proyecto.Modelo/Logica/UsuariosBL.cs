﻿using Proyecto.Modelo.Datos;
using Proyecto.Modelo.Entidades;
using Proyecto.Modelo.Utilidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Modelo.Logica
{
    internal class UsuariosBL
    {
        internal bool CrearUsuario(Usuario objUsuario, int RolId)
        {
            try
            {
                Dictionary<string, object> parametros = new Dictionary<string, object>();
                parametros.Add("psUsername", objUsuario.Username);
                parametros.Add("psPassword", new SecurityHelper().getHash(objUsuario.Password));
                parametros.Add("piRolId", RolId);
                parametros.Add("pbFotoPerfil", objUsuario.FotoPerfil);
                new Dao().EjecutarStoredProcedure(Constantes.spCrearUsuario, parametros);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        internal List<Usuario> ListarUsuarios()
        {
            string sqlString = @"SELECT [UsuarioId],[Username],[Password],[Estado]FROM[security].[Usuarios]";

            DataTable dtUsuarios = new Dao().GetDataTable(sqlString);

            List<Usuario> lstUsuarios = new List<Usuario>();
            Usuario usuario;
            foreach (DataRow fila in dtUsuarios.Rows)
            {
                usuario = new Usuario();
                usuario.UsuarioId = int.Parse(fila["UsuarioId"].ToString());
                usuario.Username = fila[1].ToString();
                usuario.Password = fila["Password"].ToString();
                usuario.Estado = bool.Parse(fila["Estado"].ToString());

                lstUsuarios.Add(usuario);
            }

            return lstUsuarios;
        }

        internal bool ValidarNombreUsuario(string username)
        {
            string sqlString = @"SELECT [UsuarioId] FROM[security].[Usuarios] WHERE [Username]='" + username + "'";

            DataTable dtUsuarios = new Dao().GetDataTable(sqlString);

            if (dtUsuarios.Rows.Count > 0)
                return false;
            else
                return true;
        }

        internal Usuario obtenerUsuario(string username)
        {
            string sqlString = @"SELECT [UsuarioId],[Username],[Password],[Estado], [FotoPerfil] FROM[security].[Usuarios] WHERE username = '" + username + "'";

            DataTable dtUsuarios = new Dao().GetDataTable(sqlString);
            Usuario usuario;
            usuario = new Usuario();
            if (dtUsuarios.Rows.Count > 0)
            {
                DataRow fila = dtUsuarios.Rows[0];

                usuario.UsuarioId = int.Parse(fila["UsuarioId"].ToString());
                usuario.Username = fila[1].ToString();
                usuario.Password = fila["Password"].ToString();
                usuario.Estado = bool.Parse(fila["Estado"].ToString());
                usuario.FotoPerfil = (byte[])fila["FotoPerfil"];

            }
            return usuario;
        }

    }
}
