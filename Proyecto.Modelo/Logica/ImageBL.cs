﻿using Proyecto.Modelo.Datos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Modelo.Logica
{
    internal class ImageBL
    {
        internal bool CrearImagen(string ruta, byte[]imagen)
        {
            try
            {
                Dictionary<string, object> parametros = new Dictionary<string, object>();
                parametros.Add("PNombre", "conejo");
                parametros.Add("PRuta", ruta);
                parametros.Add("PImagen", imagen);

                new Dao().EjecutarStoredProcedure("dbo.SaveImage", parametros);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        internal byte[] ObtenerImagen()
        {
            string sqlString = @"SELECT top 1 Imagen FROM [dbo].[Imagen]";

            DataTable dtUsuarios = new Dao().GetDataTable(sqlString);

            DataRow fila = dtUsuarios.Rows[0];

            byte[] imagen = (byte[])fila[0];
            return imagen;
        }

    }
}
