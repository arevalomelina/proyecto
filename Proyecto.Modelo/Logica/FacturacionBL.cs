﻿using Proyecto.Modelo.Datos;
using Proyecto.Modelo.Entidades;
using Proyecto.Modelo.Utilidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Modelo.Logica
{
   internal class FacturacionBL
    {
        internal Cliente GetCliente (string cedula)
        {
            string sqlString = @"SELECT [ClienteId],[cedula],[nombre],[telefono] FROM [dbo].[Cliente] WHERE ClienteId = '" + cedula + "'";

            DataTable dtCliente = new Dao().GetDataTable(sqlString);
            Cliente cliente;
            cliente = new Cliente();
            if (dtCliente.Rows.Count > 0)
            {
                DataRow fila = dtCliente.Rows[0];

                cliente.ClienteId = int.Parse(fila["ClienteId"].ToString());
                cliente.nombre = fila["nombre"].ToString();
                cliente.telefono = fila["telefono"].ToString();

            }
            return cliente;
        }

        internal List<Cliente> GetClientes()
        {
            string sqlString = @"SELECT [ClienteId],[cedula],[nombre],[telefono] FROM [dbo].[Cliente] ";

            DataTable dtCliente = new Dao().GetDataTable(sqlString);
            List<Cliente> lstClientes = new List<Cliente>();
            Cliente cliente;
            
            if (dtCliente.Rows.Count > 0)
            {
                foreach (DataRow fila in dtCliente.Rows)
                {
                    cliente = new Cliente();
                    cliente.ClienteId = int.Parse(fila["ClienteId"].ToString());
                    cliente.nombre = fila["nombre"].ToString();
                    cliente.telefono = fila["telefono"].ToString();
                    lstClientes.Add(cliente);
                }

            }
            return lstClientes;
        }

        internal List<Productos> GetProductos()
        {
            Dictionary<string, object> parametros = null;
            DataTable dtMenu = new Dao().EjecutarStoredProcedureDT(Constantes.spListarProductos, parametros);

            List<Productos> productos = new List<Productos>();
            Productos item;
            foreach (DataRow fila in dtMenu.Rows)
            {
                item = new Productos();
                item.ProductoId = int.Parse(fila["ProductoId"].ToString());
                item.Codigo = fila["Codigo"].ToString();
                item.Precio = decimal.Parse(string.IsNullOrWhiteSpace(fila["Precio"].ToString()) ? "0" : fila["Precio"].ToString());
                item.Descripcion = fila["Descripcion"].ToString();

                productos.Add(item);
            }

            return productos;
        }
    }
}
