﻿using Proyecto.Modelo.Datos;
using Proyecto.Modelo.Entidades;
using Proyecto.Modelo.Utilidades;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Modelo.Logica
{
    internal class SecurityBL
    {
        private readonly LogHelper helper = new LogHelper();
        internal bool Autenticar(string username, string password)
        {
            string hash = new Utilidades.SecurityHelper().getHash(password);
            NameValueCollection collection = new NameValueCollection();
            collection.Add("Username", username);
            collection.Add("Password", hash);

            Usuario objUsuario = SerializerHelper.GetObject<Usuario>(collection);


            helper.EscribirLogXml(LogType.info, "Se inicia proceso de logueo para el usuario " + username);

            List<LogDto> respuestaLog = helper.LeerLogXml();

            Dictionary<string, object> Parametros = new Dictionary<string, object>();
            Parametros.Add("ps_username", username);
            Parametros.Add("ps_password", hash);
            DataTable dt = new Dao().EjecutarStoredProcedureDT(Constantes.spLogin, Parametros);

            if (dt.Rows.Count > 0)
                return true;
            else
                return false;

        }

        internal List<Menu> ObtenerMenu(string username)
        {
            Dictionary<string, object> parametros = new Dictionary<string, object>();
            parametros.Add("ps_username", username);
            DataTable dtMenu = new Dao().EjecutarStoredProcedureDT(Constantes.spObtenerMenu, parametros);

            List<Menu> menu = new List<Menu>();
            Menu item;
            foreach (DataRow fila in dtMenu.Rows)
            {
                item = new Menu();
                item.AsegurableId = int.Parse(fila["AsegurableId"].ToString());
                item.Nombre = fila["Nombre"].ToString();
                item.ParentId = int.Parse(string.IsNullOrWhiteSpace(fila["ParentId"].ToString()) ? "0" : fila["ParentId"].ToString());
                item.Ruta = fila["Ruta"].ToString();

                menu.Add(item);
            }

            return menu;
        }
    }
}
