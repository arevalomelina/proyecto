﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;
using Proyecto.Modelo.Entidades;
using Proyecto.Modelo.Logica;

namespace Proyecto.Modelo
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ModeloFachada : IModeloFachada
    {
        public bool CrearUsuarios(Usuario objUsuario, int RolId)
        {
            return new UsuariosBL().CrearUsuario(objUsuario, RolId);
        }

        public List<Rol> ListarRoles()
        {
            return new RolBL().ListarRoles();
        }

        public List<Usuario> ListarUsuarios()
        {
            return new UsuariosBL().ListarUsuarios();
        }


        public bool ValidarNombreUsuario(string username)
        {
            return new UsuariosBL().ValidarNombreUsuario(username);
        }

        public Usuario obtenerUsuario(string username)
        {
            return new UsuariosBL().obtenerUsuario(username);
        }

        public bool AgregarTiposBatch(List<Tipo> lstTipos)
        {
            return new TiposBL().AgregarTiposBatch(lstTipos);
        }

        public bool AgregarTiposXml(List<Tipo> lstTipos)
        {
            return new TiposBL().AgregarTiposXml(lstTipos);
        }

        public Cliente GetCliente(string cedula)
        {
            return new FacturacionBL().GetCliente(cedula);
        }

        public List<Productos> GetProductos()
        {
            return new FacturacionBL().GetProductos();
        }

        public List<Cliente> GetClientes()
        {
            return new FacturacionBL().GetClientes();
        }
    }
}
