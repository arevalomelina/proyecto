﻿using Proyecto.Modelo.Entidades;
using Proyecto.Modelo.Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Modelo
{
    public class SecurityFachada
    {
        public bool AutenticarUsuario(string username, string password)
        {
            return new SecurityBL().Autenticar(username, password);
        }
        public List<Menu> ObtenerMenu(string username)
        {
            return new SecurityBL().ObtenerMenu(username);
        }
    }
}
