﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Modelo.Entidades
{
    public class Estudiante
    {
        public string CodigoEstudiante { get; set; }
        public string CodigoPrograma { get; set; }
        public string NombreEstudiante { get; set; }
    }
}
