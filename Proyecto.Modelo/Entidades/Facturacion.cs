﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Modelo.Entidades
{
    public class Maestro
    {
        public int ClientId { get; set; }
        public DateTime fecha { get; set; }
        public string vendedor { get; set; }
        public decimal subtotal { get; set; }
        public int ivaAplicado { get; set; }
        public decimal iva { get; set; }
        public decimal total { get; set; }
    }

    public class Detalle
    {
        public int ProductoId { get; set; }
        public int Cantidad { get; set; }
        public decimal precio { get; set; }
    }
}
