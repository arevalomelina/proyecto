﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Modelo.Entidades
{
    [Serializable]
    public class LogDto
    {
        public DateTime FechaLog { get; set; }
        public string TipoLog { get; set; }
        public string Mensaje { get; set; }

        public LogDto() { }

        public LogDto(DateTime fecha, string tipo, string mensaje)
        {
            this.FechaLog = fecha;
            this.TipoLog = tipo;
            this.Mensaje = mensaje;
        }
    }
}
