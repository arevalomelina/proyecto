﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Modelo.Entidades
{
    public class Cliente
    {
        public int ClienteId { get; set; }
        public string nombre { get; set; }
        public string telefono { get; set; }
    }

    public class Productos
    {
        public int ProductoId { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public decimal Precio { get; set; }
    }
}
