﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Modelo.Entidades
{
    public class Programa
    {
        public string CodigoPrograma { get; set; }
        public string Descripcion { get; set; }

    }
}
