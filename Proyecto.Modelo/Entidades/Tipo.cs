﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Modelo.Entidades
{
    [Serializable]
    public class Tipo
    {
        public int TipoId{get; set;}
        public string Descrpcion { get; set; }
    }
}
