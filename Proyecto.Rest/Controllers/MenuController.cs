﻿using Proyecto.Modelo;
using Proyecto.Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Proyecto.Rest.Controllers
{
    public class MenuController : ApiController
    {
        // GET: api/Menu
        public IEnumerable<Menu> Get()
        {
            return new SecurityFachada().ObtenerMenu("melina");
        }

        // GET: api/Menu/5
        public IEnumerable<Menu> Get(string username)
        {
            return new SecurityFachada().ObtenerMenu(username);
        }

        // POST: api/Menu
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Menu/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Menu/5
        public void Delete(int id)
        {
        }
    }
}
